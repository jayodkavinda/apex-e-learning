package com.learning.apex.apexelearning.controller;

import com.learning.apex.apexelearning.payload.CourseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/")
    public ResponseEntity<String> init(){
        return new ResponseEntity<>("Apex E Learning System up & Running", HttpStatus.OK);
    }
}
